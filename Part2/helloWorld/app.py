from flask import Flask

app = Flask(__name__)

app.config["DEBUG"] = True

@app.route("/")
@app.route("/hello")
def hello_world():
    return "Hello, World!"

@app.route("/test/<query>")
def search(query):
    return "You asked me about: " + query +" - That is a stupid question."


@app.route("/name/<name>")
def index(name):
    if name.lower() == "zach":
        return "Hello, {}".format(name)
    else:
        return "Not Found", 404

if __name__ == "__main__":
    app.run()
