#!/usr/bin/python3

def getnum():

    done = False
    while (not done):
        try:
            num = int(input("Enter a positive integer: "))
            done = True
        except ValueError:
            print("You did not enter a digit above, try again")

    return num

num = getnum()
if (num == 0):
    print("There are no factors for zero.")
else:
    for i in range(1,(num+1)):
        if (num % i == 0):
            print("{} is a divisor of {}".format(i,num))
