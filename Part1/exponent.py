#!/usr/bin/python3

while True:
    base     = input("Enter a base:")
    exponent = input("Enter an exponent:")

    if (base.isdigit() and exponent.isdigit()):
        break
    else:
        print("You did not enter a digit above, try again")

print("{} to the power of {} = {}"
      .format(base, exponent, int(base)**int(exponent) ))

