#!/usr/bin/python3

import pdb

def investment(amount, rate, time):
    ''' investment will track the growing amount of an investment over time
            float amount = initial investment
            float rate   = annual compounding rate
            float time   = total number of years to invest
    '''
    print("Principal: ${:.2f}".format(amount))
    print("Annual rate of return: ${:.2f}".format(rate))

    for year in range(int(time)):
        amount = amount + amount*rate
        print("Year {}: ${:.2f}".format(year, amount))

    return amount

amount  = input("Enter an initial investment amount: ")
rate    = input("Enter an annual compounding rate: ")
time    = input("Enter the total number of years to invest: ")

try:
    investment(float(amount), float(rate), float(time))
except:
    print("You did not enter a digit above, try again")

