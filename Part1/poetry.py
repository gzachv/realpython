#!/usr/bin/python3

from random import choice

def makePoem():

    nouns = ["fossil","horse","aardvark",
             "judge","chef","mango","extrovert","gorilla"]
    verbs = ["kicks","jingles","bounces","slurps","meows","explodes","curdles"]
    adjectives = ["furry","balding","incredulous","fragrant","exuberant","glistening"]
    prepositions = ["against","after","into","beneath",
                    "upon","for","in","like","over","within"]
    adverbs = ["curiously","extravagantly","tantalizingly","furiously","sensuously"]

    poem_nouns = ["","",""]
    poem_verbs = ["","",""]
    poem_adjectives = ["","",""]
    poem_prepositions = ["",""]
    poem_adverbs = [""]

    while (len(poem_nouns) != len(set(poem_nouns))):
        poem_nouns[0] = choice(nouns)
        poem_nouns[1] = choice(nouns)
        poem_nouns[2] = choice(nouns)

    while (len() != len(set())):
        [0] = choice()
        [1] = choice()
        [2] = choice()

    while (len() != len(set())):
        [0] = choice()
        [1] = choice()
        [2] = choice()

    while (len() != len(set())):
        [0] = choice()
        [1] = choice()
        [2] = choice()

    poem_adverbs[0] = choice(adverbs)

    poem =""
    return poem

print(makePoem())
